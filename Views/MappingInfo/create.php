<?php
require_once("dbcontroller.php");
$db_handle = new DBController();
$query ="SELECT * FROM courses";
$results = $db_handle->runQuery($query);

$courseList = $courseInfoObject->getCourseList();
$labList = $MappingInfoObject->getLabList();

$leadTrainerList = $TrainerInfoObject->leadTrainer();
$assistantTrainerList = $TrainerInfoObject->assistantTrainer();
$labsupporter = $TrainerInfoObject->labSupporter();
?>
<!-- <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script> -->

<script>
    function getState(val) {
        $.ajax({
            type: "POST",
            url: "MappingInfo/get_state.php",
            data:'courseName='+val,
            success: function(data){
                $("#state-list").html(data);
            }
        });
    }

    function getLt(val) {
        $.ajax({
            type: "POST",
            url: "MappingInfo/lt.php",
            data:'courseName='+val,
            success: function(data){
                $("#lt-list").html(data);
            }
        });
    }

    function getAt(val) {
        $.ajax({
            type: "POST",
            url: "MappingInfo/at.php",
            data:'courseName='+val,
            success: function(data){
                $("#at-list").html(data);
            }
        });
    }

    function getLa(val) {
        $.ajax({
            type: "POST",
            url: "MappingInfo/la.php",
            data:'courseName='+val,
            success: function(data){
                $("#la-list").html(data);
            }
        });
    }

    function selectCountry(val) {
        $("#search-box").val(val);
        $("#suggesstion-box").hide();
    }
</script>
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Assaign New Course</li>
    </ul>
</div>
<br>
<div class="tab-pane">
    <a href="?p=assigned">
        <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-grid position-left"></i> All Assaign Courses </button>
    </a> 
</div>
<br>
<!-- extra menu link -->
<form class="form-horizontal" method="POST" action="?p=assignStore">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Assaign Course</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Assaign Course </legend>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Course Name:</label>
                            <div class="col-lg-9">
                                <select id="country-list" name="courseName" data-placeholder="Select Course Name" class="select" 
                                onChange="getState(this.value); getLt(this.value); getAt(this.value); getLa(this.value) ">
                                    <option></option>
                                    <?php
                                    foreach($results as $country) {
                                        ?>
                                        <option value="<?php echo $country["unique_id"]; ?>"><?php echo $country["title"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>
                        <?php if(isset($_SESSION['courseNameErrMsg']) && !empty($_SESSION['courseNameErrMsg'])){ ?>
                            <div class="form-group">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-9">
                                    <?php echo $_SESSION['courseNameErrMsg'];
                                    unset($_SESSION['courseNameErrMsg']) ?>
                                </div>
                            </div>
                            <?php } ?>


                            <div class="form-group">
                                <label class="col-lg-3 control-label">Lab No:</label>
                                <div class="col-lg-9">
                                    <select id="state-list"  name="labNo" data-placeholder="Select Lab" class="select">     
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <?php if(isset($_SESSION['labNoErrMsg']) && !empty($_SESSION['labNoErrMsg'])){ ?>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-9">
                                        <?php echo $_SESSION['labNoErrMsg'];
                                        unset($_SESSION['labNoErrMsg']) ?>
                                    </div>
                                </div>
                                <?php } ?>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Lead Trainer:</label>
                                    <div class="col-lg-9">
                                       <select id="lt-list" name="lead_trainer" data-placeholder="Select Lead Trainer" class="select">
                                          <option></option>
                                        </select>
                                    </div>
                                </div>

                                <?php if(isset($_SESSION['lead_trainerErrMsg']) && !empty($_SESSION['lead_trainerErrMsg'])){ ?>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label"></label>
                                        <div class="col-lg-9">
                                            <?php echo $_SESSION['lead_trainerErrMsg'];
                                            unset($_SESSION['lead_trainerErrMsg']) ?>
                                        </div>
                                    </div>
                                    <?php } ?>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Assistant Trainer:</label>
                                        <div class="col-lg-9">
                                           <select id="at-list" name="asst_trainer" data-placeholder="Select Assistant Trainer" class="select">
                                              <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if(isset($_SESSION['asst_trainerErrMsg']) && !empty($_SESSION['asst_trainerErrMsg'])){ ?>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"></label>
                                            <div class="col-lg-9">
                                                <?php echo $_SESSION['asst_trainerErrMsg'];
                                                unset($_SESSION['asst_trainerErrMsg']) ?>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Lab Assistant:</label>
                                            <div class="col-lg-9">
                                               <select id="la-list" name="lab_asst" data-placeholder="Select Lab Supporter" class="select">
                                                  <option></option>
                                                </select>
                                            </div>
                                        </div>

                                        <?php if(isset($_SESSION['lab_asstErrMsg']) && !empty($_SESSION['lab_asstErrMsg'])){ ?>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label"></label>
                                                <div class="col-lg-9">
                                                    <?php echo $_SESSION['lab_asstErrMsg'];
                                                    unset($_SESSION['lab_asstErrMsg']) ?>
                                                </div>
                                            </div>
                                            <?php } ?>

                                        </fieldset>
                                    </div>

                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend class="text-semibold"> &nbsp;</legend>

                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Batch No:</label>
                                                <div class="col-lg-9">
                                                    <input name="batch_no" type="number" class="form-control" placeholder="Batch No">
                                                </div>
                                            </div>
                                            <?php if(isset($_SESSION['batch_noErrMsg']) && !empty($_SESSION['batch_noErrMsg'])){ ?>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"></label>
                                                    <div class="col-lg-9">
                                                        <?php echo $_SESSION['batch_noErrMsg'];
                                                        unset($_SESSION['batch_noErrMsg']) ?>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Course Date:</label>
                                                    <div class="col-lg-9">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                                    <input name="start_date" type="text" class="form-control pickadate-selectors" placeholder="Start Date">
                                                                </div>  

                                                                <?php if(isset($_SESSION['start_dateErrMsg']) && !empty($_SESSION['start_dateErrMsg'])){ ?>

                                                                    <div class="input-group">
                                                                        <?php echo $_SESSION['start_dateErrMsg'];
                                                                        unset($_SESSION['start_dateErrMsg']) ?>
                                                                    </div>

                                                                    <?php } ?>                                                                                                                    
                                                                </div>






                                                                <div class="col-md-6">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                                        <input name="ending_date" type="text" class="form-control pickadate-selectors" placeholder="End Date">

                                                                    </div>

                                                                    <?php if(isset($_SESSION['ending_dateErrMsg']) && !empty($_SESSION['ending_dateErrMsg'])){ ?>

                                                                        <div class="input-group">
                                                                            <?php echo $_SESSION['ending_dateErrMsg'];
                                                                            unset($_SESSION['ending_dateErrMsg']) ?>
                                                                        </div>

                                                                        <?php } ?>
                                                                    </div>





                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- trainer pc config -->
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">Course Day:</label>
                                                            <div class="col-lg-9">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="mb-15">
                                                                            <select name="day" data-placeholder="Select Day" class="select">
                                                                                <option></option>
                                                                                <option value="1">Friday</option> 
                                                                                <option value="2">Sat-Mon-Wed</option> 
                                                                                <option value="3">Sun-Tue-Thu</option>
                                                                            </select>
                                                                        </div>  

                                                                        <?php if(isset($_SESSION['dayErrMsg']) && !empty($_SESSION['dayErrMsg'])){ ?>

                                                                            <div class="mb-15">
                                                                                <?php echo $_SESSION['dayErrMsg'];
                                                                                unset($_SESSION['dayErrMsg']) ?>
                                                                            </div>

                                                                            <?php } ?>                                                                                                                      
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="form-group">
                                                                <label class="col-lg-3 control-label">Course Time:</label>
                                                                <div class="col-lg-9">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="mb-15">
                                                                                <select name="start_time" data-placeholder="Start Time" class="select">
                                                                                    <option></option>
                                                                                    <option value="8.00">8.00 AM</option> 
                                                                                    <option value="8.30">8.30 AM</option> 
                                                                                    <option value="9.00">9.00 AM</option> 
                                                                                    <option value="9.30">9.30 AM</option> 
                                                                                    <option value="10.00">10.00 AM</option> 
                                                                                    <option value="10.30">10.30 AM</option> 
                                                                                    <option value="11.00">11.00 AM</option> 
                                                                                    <option value="11.30">11.30 AM</option> 
                                                                                    <option value="12.00">12.00 PM</option> 
                                                                                    <option value="12.30">12.30 PM</option> 
                                                                                    <option value="13.00">1.00 PM</option> 
                                                                                    <option value="13.30">1.30 PM</option> 
                                                                                    <option value="14.00">2.00 PM</option> 
                                                                                    <option value="14.30">2.30 PM</option> 
                                                                                    <option value="15.00">3.00 PM</option> 
                                                                                    <option value="15.30">3.30 PM</option> 
                                                                                    <option value="16.00">4.00 PM</option> 
                                                                                    <option value="16.30">4.30 PM</option> 
                                                                                    <option value="17.00">5.00 PM</option> 
                                                                                    <option value="17.30">5.30 PM</option> 
                                                                                    <option value="18.00">6.00 PM</option> 
                                                                                    <option value="18.30">6.30 PM</option> 
                                                                                    <option value="19.00">7.00 PM</option> 
                                                                                    <option value="19.30">7.30 PM</option> 
                                                                                    <option value="20.00">8.00 PM</option> 
                                                                                    <option value="20.30">8.30 PM</option> 
                                                                                </select>
                                                                            </div>   
                                                                            <?php if(isset($_SESSION['start_timeErrMsg']) && !empty($_SESSION['start_timeErrMsg'])){ ?>

                                                                                <div class="mb-15">
                                                                                    <?php echo $_SESSION['start_timeErrMsg'];
                                                                                    unset($_SESSION['start_timeErrMsg']) ?>
                                                                                </div>

                                                                                <?php } ?>                                                                                                                    
                                                                            </div>

                                                                            <div class="col-md-6">
                                                                                <div class="mb-15">
                                                                                    <select name="ending_time" data-placeholder="End Time" class="select">
                                                                                        <option></option>
                                                                                        <option value="10.00">10.00 AM</option> 
                                                                                        <option value="10.30">10.30 AM</option> 
                                                                                        <option value="11.00">11.00 AM</option> 
                                                                                        <option value="11.30">11.30 AM</option> 
                                                                                        <option value="12.00">12.00 PM</option> 
                                                                                        <option value="12.30">12.30 PM</option> 
                                                                                        <option value="13.00">1.00 PM</option> 
                                                                                        <option value="13.30">1.30 PM</option> 
                                                                                        <option value="14.00">2.00 PM</option> 
                                                                                        <option value="14.30">2.30 PM</option> 
                                                                                        <option value="15.00">3.00 PM</option> 
                                                                                        <option value="15.30">3.30 PM</option> 
                                                                                        <option value="16.00">4.00 PM</option> 
                                                                                        <option value="16.30">4.30 PM</option> 
                                                                                        <option value="17.00">5.00 PM</option> 
                                                                                        <option value="17.30">5.30 PM</option> 
                                                                                        <option value="18.00">6.00 PM</option> 
                                                                                        <option value="18.30">6.30 PM</option> 
                                                                                        <option value="19.00">7.00 PM</option> 
                                                                                        <option value="19.30">7.30 PM</option> 
                                                                                        <option value="20.00">8.00 PM</option> 
                                                                                        <option value="20.30">8.30 PM</option> 
                                                                                        <option value="21.00">9.00 PM</option> 
                                                                                    </select>
                                                                                </div>

                                                                                <?php if(isset($_SESSION['ending_timeErrMsg']) && !empty($_SESSION['ending_timeErrMsg'])){ ?>

                                                                                    <div class="mb-15">
                                                                                        <?php echo $_SESSION['ending_timeErrMsg'];
                                                                                        unset($_SESSION['ending_timeErrMsg']) ?>
                                                                                    </div>

                                                                                    <?php } ?>
                                                                                </div>








                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </fieldset>
                                                            </div>
                                                        </div>

                                                        <div class="text-right">
                                                            <button type="submit" name="assignCourse" class="btn btn-primary">Assaign Course <i class="icon-arrow-right14 position-right"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- /2 columns form -->

                                            <script type="text/javascript">
                                                $('.pickadate-selectors').pickadate();
                                            </script>