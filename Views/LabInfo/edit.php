<?php
$editLabInfo = $labInfoObject->assign($_REQUEST)->show();
$data = $courseInfoObject->getCourseList();
$_REQUEST['courseName']=$editLabInfo['course_id'];
$getCourseName = $courseInfoObject->assign($_REQUEST)->getCourseName();
$pcc = unserialize($editLabInfo['pc_configuration']);
$tpcc = unserialize($editLabInfo['trainer_pc_configuration']);

$_SESSION['labNo'] = $editLabInfo['lab_no'];
?>
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
        <li><a href="?p=allLab">All Lab</a></li>
        <li class="active">Edit Lab</li>
    </ul>
</div>
<br>

<div class="tab-pane"> 
    <a href="?p=allLab">
        <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add New Lab</button>
    </a>        
    <a href="?p=allLab">
        <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-grid position-left"></i> Show All Lab</button>
    </a> 
</div>
<br>

<?php if(!empty($editLabInfo )){ ?>
    <form class="form-horizontal" method="POST" action="?p=labUpdate">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Edit Lab Information</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Edit Lab </legend>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Course Name:</label>
                            <div class="col-lg-9">
                                <select name="courseName" data-placeholder="Select Course Name" class="select">
                                    <option></option>
                                    <option value="<?php echo $editLabInfo['course_id'] ?>" selected><?php echo $getCourseName['title']  ?></option>

                                    <?php
                                    if (!empty($data)) {
                                        foreach ($data as $value) {
                                            ?>
                                            <option value="<?php echo $value['unique_id'] ?>">
                                                <?php echo $value['title'] ?></option>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php if(isset($_SESSION['courseNameErrMsg']) && !empty($_SESSION['courseNameErrMsg'])){ ?>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-9">
                                        <?php echo $_SESSION['courseNameErrMsg'];
                                        unset($_SESSION['courseNameErrMsg']) ?>
                                    </div>
                                </div>
                                <?php } ?>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Lab No:</label>
                                    <div class="col-lg-9">
                                        <input name="labNo" type="number" class="form-control" 
                                        value="<?php echo $editLabInfo['lab_no'] ?>" placeholder="Lab No" disabled>
                                    </div>
                                </div>
                                <?php if(isset($_SESSION['labNoErrMsg']) && !empty($_SESSION['labNoErrMsg'])){ ?>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label"></label>
                                        <div class="col-lg-9">
                                            <?php echo $_SESSION['labNoErrMsg'];
                                            unset($_SESSION['labNoErrMsg']) ?>
                                        </div>
                                    </div>
                                    <?php } ?>


                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Seat Capacity:</label>
                                        <div class="col-lg-9">
                                            <input name="seatCapacity" type="number" class="form-control" placeholder="Seat Capacity" value="<?php echo $editLabInfo['seat_capacity'] ?>">
                                        </div>
                                    </div>

                                    <?php if(isset($_SESSION['seatCapacityErrMsg']) && !empty($_SESSION['seatCapacityErrMsg'])){ ?>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"></label>
                                            <div class="col-lg-9">
                                                <?php echo $_SESSION['seatCapacityErrMsg'];
                                                unset($_SESSION['seatCapacityErrMsg']) ?>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Table Capacity:</label>
                                            <div class="col-lg-9">
                                                <input name="tableCapacity" type="number" class="form-control" placeholder="Table Capacity" value="<?php echo $editLabInfo['table_capacity'] ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Projector Resolution:</label>
                                            <div class="col-lg-9">
                                                <input name="proResolution" type="text" class="form-control" placeholder="Ex:  1024x768" value="<?php echo $editLabInfo['projector_resolution'] ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">AC Status:</label>
                                            <div class="col-lg-9">
                                                <div class="row">

                                                    <?php if($editLabInfo['ac_status'] < 1){ ?>
                                                        <div class="col-md-6">
                                                            <div class="mb-15">
                                                                <select onchange="acStatus(this);" name="acStauts" data-placeholder="Select AC Status" class="select">
                                                                    <option></option>
                                                                    <option value="yes">Yes</option> 
                                                                    <option value="no" selected>No</option> 
                                                                </select>
                                                            </div>                                                
                                                        </div>
                                                        <div class="col-md-6" id="acStatus" style="display: none;">
                                                            <input name="acStauts" type="text" placeholder="How many AC" class="form-control mb-15" value="0" required>

                                                        </div>

                                                        <?php }else{ ?>
                                                            <div class="col-md-6">
                                                                <div class="mb-15">
                                                                    <select onchange="acStatus(this);" name="acStauts" data-placeholder="Select AC Status" class="select">
                                                                        <option></option>
                                                                        <option value="yes" selected>Yes</option> 
                                                                        <option value="no">No</option> 
                                                                    </select>
                                                                </div>                                                
                                                            </div>
                                                            <div class="col-md-6" id="acStatus" style="display: block;">
                                                                <input id="cv" name="acStauts" type="text" placeholder="How many AC" class="form-control mb-15" value="<?php echo $editLabInfo['ac_status'] ?>" required>

                                                            </div>
                                                            <?php } ?>




                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </div>

                                        <div class="col-md-6">
                                            <fieldset>
                                                <legend class="text-semibold"> &nbsp;</legend>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Student PC Configuration:</label>
                                                    <div class="col-lg-9">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="mb-15">
                                                                    <select name="processor" data-placeholder="Select Processor" class="select">
                                                                        <option></option>
                                                                        <option selected><?php echo ucwords($pcc['processor']) ?></option>
                                                                        <option value="celeron">Celeron</option> 
                                                                        <option value="core 2 due">Core 2 Due</option> 
                                                                        <option value="core 2 quad">Core 2 Quad</option> 
                                                                        <option value="core i5">Core i5</option> 
                                                                        <option value="core i7">Core i7</option> 
                                                                    </select>
                                                                </div>                                                                                                                       
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="mb-15">
                                                                    <select name="ram" data-placeholder="Select RAM" class="select">
                                                                        <option></option>
                                                                        <option selected><?php echo strtoupper($pcc['ram']); ?></option>
                                                                        <option value="1 gb">1 GB</option> 
                                                                        <option value="2 gb">2 GB</option> 
                                                                        <option value="4 gb">4 GB</option> 
                                                                        <option value="8 gb">8 GB</option> 
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="mb-15">
                                                                    <select name="brand" data-placeholder="Select Brand" class="select">
                                                                        <option></option>
                                                                        <option selected><?php echo ucwords($pcc['brand']) ?></option>
                                                                        <option value="dell">Dell</option> 
                                                                        <option value="sumsang">Sumsang</option> 
                                                                        <option value="lg">LG</option> 
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="mb-15">
                                                                    <select name="os" data-placeholder="Select OS" class="select">
                                                                        <option></option>
                                                                        <option selected><?php echo ucwords($pcc['os']) ?></option>
                                                                        <option value="windows 7">Windows 7</option> 
                                                                        <option value="windows 8">Windows 8</option> 
                                                                        <option value="windows 8.1">Windows 8.1</option> 
                                                                        <option value="windows 10">Windows 10</option> 
                                                                        <option value="redhat linux">RedHat Linux</option> 
                                                                        <option value="cent os">Cent OS Linux</option> 
                                                                        <option value="mac">Mac</option> 
                                                                    </select>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- trainer pc config -->
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Trainer PC Configuration:</label>
                                                    <div class="col-lg-9">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="mb-15">
                                                                    <select name="tprocessor" data-placeholder="Select Processor" class="select">
                                                                        <option></option>
                                                                        <option selected><?php echo ucwords($tpcc['processor']) ?></option>
                                                                        <option value="celeron">Celeron</option> 
                                                                        <option value="core 2 due">Core 2 Due</option> 
                                                                        <option value="core 2 quad">Core 2 Quad</option> 
                                                                        <option value="core i5">Core i5</option> 
                                                                        <option value="core i7">Core i7</option> 
                                                                    </select>
                                                                </div>                                                                                                                       
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="mb-15">
                                                                    <select name="tram" data-placeholder="Select RAM" class="select">
                                                                        <option></option>
                                                                        <option selected><?php echo strtoupper($tpcc['ram']); ?></option>
                                                                        <option value="1 gb">1 GB</option> 
                                                                        <option value="2 gb">2 GB</option> 
                                                                        <option value="4 gb">4 GB</option> 
                                                                        <option value="8 gb">8 GB</option> 
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="mb-15">
                                                                    <select name="tbrand" data-placeholder="Select Brand" class="select">
                                                                        <option></option>
                                                                        <option selected><?php echo ucwords($tpcc['brand']) ?></option>
                                                                        <option value="dell">Dell</option> 
                                                                        <option value="sumsang">Sumsang</option> 
                                                                        <option value="lg">LG</option> 
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="mb-15">
                                                                    <select name="tos" data-placeholder="Select OS" class="select">
                                                                        <option></option>
                                                                        <option selected><?php echo ucwords($tpcc['os']) ?></option>
                                                                        <option value="windows 7">Windows 7</option> 
                                                                        <option value="windows 8">Windows 8</option> 
                                                                        <option value="windows 8.1">Windows 8.1</option> 
                                                                        <option value="windows 10">Windows 10</option> 
                                                                        <option value="redhat linux">RedHat Linux</option> 
                                                                        <option value="cent os">Cent OS Linux</option> 
                                                                        <option value="mac">Mac</option> 
                                                                    </select>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Internet Speed:</label>
                                                    <div class="col-lg-9">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="mb-15">
                                                                    <select name="internetSpeed" data-placeholder="Select Internet Speed" class="select">
                                                                        <option></option>
                                                                        <option selected><?php echo $editLabInfo['internet_speed'] ?></option>
                                                                        <option value="256 KB">256 KB</option> 
                                                                        <option value="512 KB">512 KB</option> 
                                                                        <option value="1 MB">1 MB</option> 
                                                                        <option value="2 MB">2 MB</option> 
                                                                        <option value="5 MB">5 MB</option> 
                                                                    </select>
                                                                </div>                                                                                                                       
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>


                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="submit" name="addLab" class="btn btn-primary">Update Lab <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- /2 columns form -->


                        <script>
                            function acStatus(that) {
                                if (that.value == "yes") {
                                    document.getElementById("acStatus").style.display = "block";

                                } else {
                                    document.getElementById("acStatus").style.display = "none";
                                    document.getElementById("cv").value = "0";

                                }
                            }
                        </script>

                    <?php }else{ ?>
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">No Editable Lab</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <?php } ?>

                 

