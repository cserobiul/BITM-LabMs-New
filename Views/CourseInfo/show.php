
<?php
$data = $courseInfoObject->assign($_REQUEST)->show();

?>

<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
        <li><a href="?p=allCourses">All Courses</a></li>
        <li class="active">Course Details</li>
    </ul>
</div>
<br>

	<div class="tab-pane">
		<a href="?p=addCourse">
			<button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add Course</button>
		</a> 
		<a href="?p=allCourses">
			<button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-grid position-left"></i>  All  Courses</button>
		</a> 
	</div>
	<br>
	<!-- extra menu link -->

	<!-- Invoice template -->
	<?php if(!empty($data)){ 
		?>
		<div class="panel panel-white">
			<div class="panel-heading">
				<h4 class="panel-title">Courses Details Information </h4>
				<div class="heading-elements">
					<a href="?p=editCourse&id=<?php echo $data['unique_id'] ?>">
						<button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-pencil position-left"></i> Edit</button>
					</a>
					<button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-printer position-left"></i> Print</button>
				</div>
			</div>



			<div class="table-responsive">
				<table class="table table-lg">

					<tbody>

						<tr>
							<td>
								<h6 class="no-margin">Course Name</h6>                                
							</td>
							<td><?php echo $data['title'] ?>

							</td>
						</tr>
						<tr>
							<td>
								<h6 class="no-margin">Course Duration</h6>                                
							</td>
							<td><?php echo $data['duration'] ?> </td>
						</tr>
						<tr>
							<td>
								<h6 class="no-margin">Course Description</h6>                                
							</td>
							<td><?php echo ucwords($data['description']) ?></td>
						</tr>
						<tr>
							<td>
								<h6 class="no-margin">Course Type</h6>                                
							</td>
							<td><?php 
							if($data['course_type'] == 'paid'){
								echo ucwords($data['course_type']). ' ['. $data['course_fee'] .' TK]';
							}else{
								echo ucwords($data['course_type']);
							}  ?></td>
						</tr>
						<tr>
							<td>
								<h6 class="no-margin">Course Start at</h6>                                
							</td>
							<td><?php 
                                $originalDate = $data['created'];
                                $newDate = date("d-M-Y", strtotime($originalDate));
                                echo $newDate; ?>  </td>
						</tr>
						


								</tbody>
							</table>
						</div>


					</div>

					<?php }else{ 
						echo '<script type="text/javascript">location.replace("?p=404");</script>';
					} ?>



