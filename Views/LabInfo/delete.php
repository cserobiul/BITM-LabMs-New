<?php 
$data = $labInfoObject->assign($_REQUEST)->delete();

if($data == 1){
	echo '<script type="text/javascript">location.replace("?p=allLab");</script>';
}else{ ?>
	<div class="breadcrumb-line">
        <a href="?p=addLab">
            <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add New Lab</button>
        </a> 
        <a href="?p=allLab">
            <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-grid position-left"></i> Show All Lab</button>
        </a> 
    </div>
    <br>
	
	<div class="panel panel-flat">
		<div class="panel-heading">
		<h4 class="panel-title">No Deleteable Info</h4>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>                
				</ul>
			</div>
		</div>
	</div>
	<?php }
	?>
