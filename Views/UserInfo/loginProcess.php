<?php

include_once('../../vendor/autoload.php');
$loginObj = new \Apps\LabInfo\labinfo();

if(!empty($_REQUEST['username']) && !empty($_REQUEST['password'])){
	$logData = $loginObj->assign($_REQUEST)->login();

	if($logData>0){
		$_SESSION['loginUser'] = $logData['username'];
		$_SESSION['uID'] = $logData['unique_id'];
		$_SESSION['is_admin'] = $logData['is_admin'];
		header("location:../index.php");
	}else{
		$_SESSION['logMsg'] = "* Username or Password Did not match";
		echo '<script type="text/javascript">location.replace("../../index.php");</script>';
	}

}else{
	$_SESSION['logMsg'] = "* Both filed are required";
	echo '<script type="text/javascript">location.replace("../../index.php");</script>';
}