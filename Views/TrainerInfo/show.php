
<?php
$data = $TrainerInfoObject->assign($_REQUEST)->show();

?>

<div class="breadcrumb-line">
	<ul class="breadcrumb">
		<li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
		<li><a href="?p=allTrainer">All Trainer</a></li>
		<li class="active">Trainer Details</li>
	</ul>
</div>
<br>
<div class="tab-pane">
	<a href="?p=addTrainer">
		<button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add New Trainer</button>
	</a> 
	<a href="?p=allTrainer">
		<button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-grid position-left"></i> Show All Trainer's</button>
	</a> 
	<br>
	<br>
</div>

<?php 
if (!empty($data)) { ?>
	<div class="row">
		<div class="col-lg-9">			
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title">Trainer Training Info</h5>
					<div class="heading-elements">
						<ul class="icons-list">
							<li><a data-action="collapse"></a></li>
							<div class="heading-elements">
								<button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-printer position-left"></i> Print</button>
							</div>
						</ul>
					</div>
				</div>

				<div class="panel-body">
					<div class="table-responsive">
						<table class="table datatable-basic">
							<tbody>
								<tr>
									<td>
										<h6 class="no-margin">Name </h6>                                
									</td>
									<td><?php echo ucwords($data['full_name']);?></td>
								</tr>
								
								<tr>
									<td>
										<h6 class="no-margin">Team </h6>                                
									</td>
									<td><?php echo ucwords($data['team']) ?> </td>
								</tr>
								<tr>
									<td>
										<h6 class="no-margin">Course Name</h6>                                
									</td>
									<td><?php 
										$_REQUEST['courseName'] = $data['course_name'];
										$courseName = $courseInfoObject->assign($_REQUEST)->getCourseName();
										echo $courseName['title']?>  </td>
									</tr>

									<tr>
										<td>
											<h6 class="no-margin">Trainer Level</h6>                                
										</td>
										<td><?php 
											if($data['trainer_level'] == 1){
												echo 'Lead Trainer';
											}elseif ($data['trainer_level'] == 2) {
												echo 'Assistant Trainer';
											}elseif ($data['trainer_level'] == 3) {
												echo 'Lab Assistant';
											}else{
												echo 'NA';
											}
											?></td>
										</tr>
										<tr>
											<td>
												<h6 class="no-margin">Join At</h6>                                
											</td>
											<td><?php 
												$originalDate = $data['created'];
												$newDate = date("d-M-Y", strtotime($originalDate));
												echo $newDate; ?>  </td>
											</tr>				
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /profile info -->
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Trainer Personal Details</h5>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div>

							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-lg">
										<tbody>
											<tr>
												<td>
													<h6 class="no-margin">Education Status</h6>                                
												</td>
												<td><?php echo ucwords($data['edu_status']) ?> </td>
											</tr>
											<tr>
												<td>
													<h6 class="no-margin">Phone </h6>                                
												</td>
												<td><?php echo $data['phone'];?></td>
											</tr>
											<tr>
												<td>
													<h6 class="no-margin">Email</h6>                                
												</td>
												<td><?php echo $data['email'] ?> </td>
											</tr>
											<tr>
												<td>
													<h6 class="no-margin">Address </h6>                                
												</td>
												<td><?php echo ucwords($data['address']) ?> </td>
											</tr>
											<tr>
												<td>
													<h6 class="no-margin">Gender</h6>                                
												</td>
												<td><?php echo ucwords($data['gender']) ?>  </td>
											</tr>
											<tr>
												<td>
													<h6 class="no-margin">Web Address</h6>                                
												</td>
												<td><?php echo $data['web'] ?></td>
											</tr>
											<tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="thumbnail">
								<div class="thumb thumb-rounded thumb-slide">
									<img src="assets/images/placeholder.jpg" alt="">				
								</div>
								<div class="caption text-center">
									<h6 class="text-semibold no-margin">
										<?php echo $data['full_name'];?> 
										<small class="display-block">
											<?php 
											if($data['trainer_level'] == 1){
												echo 'Lead Trainer';
											}elseif ($data['trainer_level'] == 2) {
												echo 'Assistant Trainer';
											}elseif ($data['trainer_level'] == 3) {
												echo 'Lab Assistant';
											}else{
												echo 'NA';
											}
											?>

										</small></h6>
									</div>
								</div>
								<!-- /user thumbnail -->
							</div>
						</div>
						<?php }else{?>
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">No Trainer's Details</h6>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
							</div>
							<?php } ?>